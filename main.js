document.getElementById('task-form').addEventListener('submit', function(event) {
    event.preventDefault();
    const taskInput = document.getElementById('task-input');
    const taskList = document.getElementById('task-list');
    if (taskInput.value) {
        const li = document.createElement('li');
        li.className = 'flex justify-between p-2 border-b';
        li.innerHTML = `
            <span class="task">${taskInput.value}</span>
            <div>
                <button class="complete-btn text-green-500 mr-2"><i class="fas fa-check"></i></button>
                <button class="delete-btn text-red-500"><i class="fas fa-trash"></i></button>
            </div>
        `;
        taskList.appendChild(li);
        taskInput.value = '';
    }
});

document.getElementById('task-list').addEventListener('click', function(event) {
    if (event.target.closest('.delete-btn')) {
        event.target.closest('li').remove();
    } else if (event.target.closest('.complete-btn')) {
        event.target.closest('li').querySelector('.task').classList.toggle('line-through');
    }
});
